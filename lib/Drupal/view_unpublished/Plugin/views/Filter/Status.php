<?php

/**
 * @file
 * Contains \Drupal\view_unpublished\Plugin\views\Filter\Status.
 */

namespace Drupal\view_unpublished\Plugin\views\Filter;

use Drupal\node\Plugin\views\filter\Status as NodeStatus;

/**
 * Filter by view all unpublished permissions granted by view_unpublished.
 *
 * @ingroup views_filter_handlers
 *
 * @PluginID("view_unpublished_node_status")
 */
class Status extends NodeStatus {

  public function query() {
    $table = $this->ensureMyTable();
    $where_per_type = array();
    foreach (node_type_get_types() as $type => $name) {
      $where_per_type[] = "($table.type = '$type' AND ***VIEWUNPUBLISHED_$type*** = 1)";
    }
    $where_per_type = implode(' OR ', $where_per_type);
    $this->query->addWhereExpression($this->options['group'], "$table.status = 1 OR ($table.uid = ***CURRENT_USER*** AND ***CURRENT_USER*** <> 0 AND ***VIEW_OWN_UNPUBLISHED_NODES*** = 1) OR ***ADMINISTER_NODES*** = 1 OR $where_per_type");
  }

}
